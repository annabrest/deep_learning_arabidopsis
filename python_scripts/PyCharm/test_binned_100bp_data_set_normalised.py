#!/usr/bin/env python
# coding: utf-8

# In[1]:
# Load packages:

from __future__ import absolute_import, division, print_function, unicode_literals
import functools
import tensorflow as tf
import numpy as np
import pandas as pd
import glob #if you want to read files from global invironment
from sklearn.model_selection import train_test_split


# In[2]:
# Read file with labels
path_to_output_data = "/Users/abrestovitsky/Work/2019/Analysis/Deep_learning_arabidopsis/output_data/"
file_name_labels = "CNN_expression_output_mean_apex_inflorescence_21d_AGI.csv"

train_set_y_orig = pd.read_csv(path_to_output_data + file_name_labels)
type(train_set_y_orig)
# Out[21]: pandas.core.frame.DataFrame

# In[3]:

print(train_set_y_orig.shape)
train_set_y_orig.head(3)
# (21821, 2)
#          AGI  mean_apex_inflorescence_21d
# 0  AT1G01010                            0
# 1  AT1G01020                            1
# 2  AT1G01030                            0



# In[4]:
# Read Input files

file_names=[s + ".csv" for s in np.array(train_set_y_orig.AGI)]


# In[5]:


folder_name_input = "CNN_input_matrix_df_bin_100bp/"

file_list = [path_to_output_data + folder_name_input + s for s in file_names]


#file_list = (path + file_names)
print(file_list[0:5])
# ['/Users/abrestovitsky/Work/2019/Analysis/Deep_learning_arabidopsis/output_data/CNN_input_matrix_df_bin_100bp/AT1G01010.csv',
# '/Users/abrestovitsky/Work/2019/Analysis/Deep_learning_arabidopsis/output_data/CNN_input_matrix_df_bin_100bp/AT1G01020.csv',
# '/Users/abrestovitsky/Work/2019/Analysis/Deep_learning_arabidopsis/output_data/CNN_input_matrix_df_bin_100bp/AT1G01030.csv',
# '/Users/abrestovitsky/Work/2019/Analysis/Deep_learning_arabidopsis/output_data/CNN_input_matrix_df_bin_100bp/AT1G01040.csv',
# '/Users/abrestovitsky/Work/2019/Analysis/Deep_learning_arabidopsis/output_data/CNN_input_matrix_df_bin_100bp/AT1G01060.csv']

# In[6]:


dfs=np.array([pd.read_csv(fp).values for fp in file_list])
print(dfs.shape)
# (21821, 872, 15)


# In[78]:


dfs[:,(1,2,4),:].shape


# In[83]:
# Filter out TFs bound to more than 15000 genes
# Based on the calculation that have been done using R

TF_index = pd.read_csv(path_to_output_data + "CNN_input_matrix_df/TF_filtered_15000_index.csv")
TF_index.columns = ["index"]

#%%
print(type(TF_index))
print(TF_index.head(3))
print(dfs[:,TF_index.index,:].shape)
# (21821, 825, 15)

# In[90]:


dfs_filtered = dfs[:,TF_index.index,:]
dfs_filtered[1]

# In[8]:

row_n = dfs_filtered.shape[1]
col_n = dfs_filtered.shape[2]

# Define training set
Y_train = (np.array(train_set_y_orig['mean_apex_inflorescence_21d'])).reshape(train_set_y_orig['mean_apex_inflorescence_21d'].shape[0], 1)
X_train = dfs_filtered.reshape((train_set_y_orig.shape[0]), row_n, col_n, 1)


# In[9]:

print ("number of training examples = " + str(X_train.shape[0]))
print ("X_train shape: " + str(X_train.shape))
print ("Y_train shape: " + str(Y_train.shape))
# number of training examples = 21821
# X_train shape: (21821, 825, 15, 1)
# Y_train shape: (21821, 1)



#%% Split data set inti "train" and "test":
# set the seed by "random_state=11"

#x_train, x_test, y_train, y_test = train_test_split(X_train, Y_train, test_size = 0.2)
#next time set the seed

x_train, x_test, y_train, y_test = train_test_split(X_train, Y_train, test_size = 0.2, random_state=11) #set the seed by "random_state=11"

# acheck new dim of the data set after split:
print ("number of training examples = " + str(x_train.shape[0]))
print ("x_train shape: " + str(x_train.shape))
print ("y_train shape: " + str(y_train.shape))

# number of training examples = 17456
# x_train shape: (17456, 825, 15, 1)
# y_train shape: (17456, 1)


# In[10]:
# example of using ImageDataGenerator to normalize images

from keras.utils import to_categorical
from keras.models import Sequential
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Dense
from keras.layers import Flatten
from keras.preprocessing.image import ImageDataGenerator


# In[15]:

#######################
trainX = x_train
testX = x_test
trainY = y_train
testY = y_test


# In[16]:


# confirm scale of pixels
print('Train min=%.3f, max=%.3f' % (trainX.min(), trainX.max()))
print('Test min=%.3f, max=%.3f' % (testX.min(), testX.max()))
# Train min=0.000, max=99.996
# Test min=0.000, max=99.996

# In[17]:


# create generator (1.0/99.996 = 0.010000400016000641)
datagen = ImageDataGenerator(rescale=1.0/99.996)


# In[18]:


# prepare an iterators to scale images
train_iterator = datagen.flow(trainX, trainY, batch_size=64)
test_iterator = datagen.flow(testX, testY, batch_size=64)
print('Batches train=%d, test=%d' % (len(train_iterator), len(test_iterator)))
# Batches train=273, test=69

# In[19]:


# confirm the scaling works
batchX, batchY = train_iterator.next()
print('Batch shape=%s, min=%.3f, max=%.3f' % (batchX.shape, batchX.min(), batchX.max()))


# In[32]:


# define model
model = Sequential()
model.add(Conv2D(32, (3, 3), activation='relu', input_shape=(row_n, 15, 1)))
model.add(MaxPooling2D((2, 2)))
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(MaxPooling2D((2, 2)))
model.add(Flatten())
model.add(Dense(64, activation='relu'))
model.add(Dense(1, activation='sigmoid'))


# In[35]:


# compile model
model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])


# In[36]:


# fit model with generator
model.fit_generator(train_iterator, steps_per_epoch=len(train_iterator), epochs=5)


# In[37]:


# evaluate model
_, acc = model.evaluate_generator(test_iterator, steps=len(test_iterator), verbose=0)
print('Test Accuracy: %.3f' % (acc * 100))
# Test Accuracy: 57.365

######################
######################-------------------------

# In[ ]:


# fit model with generator
# model.fit_generator(train_iterator, steps_per_epoch=len(train_iterator), epochs=5)


# In[21]:


model=Sequential()
model.add(Conv2D(filters=32, kernel_size=(row_n, 1), strides=1, activation='relu', input_shape=(row_n, 15, 1)))
model.add(MaxPooling2D(pool_size=(1,3)))
model.add(Flatten())
model.add(Dense(64, activation='relu'))

model.add(Dense(1, activation='sigmoid')) # changed

# compile model
model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

# fit model with generator
model.fit_generator(train_iterator, steps_per_epoch=len(train_iterator), epochs=5)

# evaluate model
_, acc = model.evaluate_generator(test_iterator, steps=len(test_iterator), verbose=0)
print('Test Accuracy: %.3f' % (acc * 100))
#Test Accuracy: 56.816









# In[29]:



# fit model with generator
model.fit_generator(train_iterator, steps_per_epoch=len(train_iterator), epochs=20)
# 273/273 [==============================] - 14s 51ms/step - loss: 0.0427 - accuracy: 0.9859

# evaluate model
_, acc = model.evaluate_generator(test_iterator, steps=len(test_iterator), verbose=0)
print('Test Accuracy: %.3f' % (acc * 100))

#Test Accuracy: 55.899






################
#%%
# MODEL 1
#%% Define parameters
kernel_size = (row_n, 1)
n_filters = 32
x_train_shape = x_train.shape[1:4]
Conv2D_dim_1 = (1, 3)
Conv2D_n_1 = 64
pool_size_1 = (1,3)
Dense_n_1 = 64
my_optimizer = 'adam'
my_loss = 'binary_crossentropy'
my_epochs = 16
# In[47]:


model=Sequential()
model.add(Conv2D(n_filters, kernel_size, activation='relu', input_shape=x_train_shape))
model.add(Conv2D(Conv2D_n_1, Conv2D_dim_1, activation='relu'))
model.add(MaxPooling2D(pool_size=pool_size_1))
model.add(Flatten())
model.add(Dense(Dense_n_1, activation='relu'))
model.add(Dense(1, activation='sigmoid')) # changed

# compile model
model.compile(optimizer=my_optimizer, loss=my_loss, metrics=['accuracy'])


# In[48]:


# fit model with generator
model.fit_generator(train_iterator, steps_per_epoch=len(train_iterator), epochs=my_epochs)
# 269/273 [============================>.] - ETA: 0s - loss: 0.0482 - accuracy: 0.9864
# 270/273 [============================>.] - ETA: 0s - loss: 0.0482 - accuracy: 0.9864
# 271/273 [============================>.] - ETA: 0s - loss: 0.0482 - accuracy: 0.9864
# 272/273 [============================>.] - ETA: 0s - loss: 0.0482 - accuracy: 0.9864
# 273/273 [==============================] - 18s 65ms/step - loss: 0.0481 - accuracy: 0.9864

# In[49]:


# evaluate model
_, acc = model.evaluate_generator(test_iterator, steps=len(test_iterator), verbose=0)
print('Test Accuracy: %.3f' % (acc * 100))
# Test Accuracy: 55.349

# In[ ]:


#%%
# initialize list of lists
data = [
['kernel_size', kernel_size],
['n_filters', n_filters],
['x_train_shape', x_train_shape],
['Conv2D_dim_1', Conv2D_dim_1],
['Conv2D_n_1', Conv2D_n_1],
['pool_size_1', pool_size_1],
['Dense_n_1', Dense_n_1],
['my_optimizer', 'my_optimizer'],
['my_loss', 'my_loss'],
['my_epochs', my_epochs],
['Test Accuracy', acc * 100]
]
# Create the pandas DataFrame
parameters_model_1 = pd.DataFrame(data, columns=['parameter', 'value'])
parameters_model_1
#%%
# MODEL 1-THE END
#%%
#%%
# MODEL 2
#%%

#%% Define parameters
kernel_size = (row_n, 1)
n_filters = 32
x_train_shape = x_train.shape[1:4]
Conv2D_dim_1 = (1, 3)
Conv2D_n_1 = 64
pool_size_1 = (1,3)
Dense_n_1 = 64
my_optimizer = 'adam'
my_loss = 'binary_crossentropy'
# tf.keras.losses.BinaryCrossentropy()
my_epochs = 16
# In[68]:


from keras.models import Sequential
from keras.layers import *

model = Sequential()
model.add(
    Conv2D(filters=n_filters, kernel_size=kernel_size, strides=1, activation='relu', input_shape=x_train_shape, use_bias=True))
model.add(MaxPooling2D(pool_size=Conv2D_dim_1))
model.add(Flatten())
model.add(Dense(Dense_n_1, activation='relu'))
#
# model.add(Dense(200, activation='relu',
#              kernel_regularizer=regularizers.l2(0.01),
#              activity_regularizer=regularizers.l1(0.01)))
# model.add(Dense(32, activation='relu'))
# model.add(Dense(16, activation='relu'))
model.add(Dense(1, activation='sigmoid'))  # changed


# model.compile(optimizer='adam', loss='mse', metrics = ["accuracy"])
# model.compile(optimizer='sgd', loss=tf.keras.losses.BinaryCrossentropy(), metrics = ["accuracy"])
model.compile(optimizer=my_optimizer, loss=my_loss, metrics=["accuracy"])

# In[69]:

history = model.fit(x_train, y_train, epochs=my_epochs, verbose=1, shuffle=True, validation_split=0.2)
# 12791/12791 [==============================] - 2s 141us/step - loss: 0.0841 - accuracy: 0.9694 - val_loss: 2.5930 - val_accuracy: 0.5419
# In[70]:


history = model.fit(x_train, y_train, epochs=100, verbose=1, shuffle=True, validation_split=0.2)

# In[80]:


history.history.keys()
# Out[88]: dict_keys(['val_loss', 'val_accuracy', 'loss', 'accuracy'])
history.history['accuracy']


# In[77]:


import matplotlib.pyplot as plt
#get_ipython().run_line_magic('matplotlib', 'inline')
#%%
# Plot training & validation accuracy values
plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
plt.title('Model_2 accuracy')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.savefig("Accuracy_model_2.png")
plt.show()
#%%
# Plot training & validation loss values
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('Model_2 loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.savefig("Loss_model_2.png")
plt.show()

#%%
#%%
#%%
# MODEL 3
#%%

#%% Define parameters
kernel_size = (row_n, 1)
n_filters = 32
x_train_shape = x_train.shape[1:4]
Conv2D_dim_1 = (1, 3)
Conv2D_n_1 = 64
pool_size_1 = (1,3)
Dense_n_1 = 64
my_optimizer = 'adam'
my_loss = 'binary_crossentropy'
kernal_weight_decay = 0.01
activity_weight_decay = 0.01

my_epochs = 16

# In[68]:


from keras.models import Sequential
from keras.layers import *
from tensorflow.keras import regularizers

model = Sequential()
model.add(
    Conv2D(filters=n_filters, kernel_size=kernel_size, strides=1, activation='relu', input_shape=x_train_shape, use_bias=True))
model.add(MaxPooling2D(pool_size=Conv2D_dim_1))
model.add(Flatten())

model.add(Dense(Dense_n_1, activation='relu',
             kernel_regularizer= regularizers.l2(kernal_weight_decay),
             activity_regularizer= regularizers.l1(activity_weight_decay)))
model.add(Dropout(0.25))

model.add(Dense(1, activation='sigmoid'))  # changed


# model.compile(optimizer='adam', loss='mse', metrics = ["accuracy"])
# model.compile(optimizer='sgd', loss=tf.keras.losses.BinaryCrossentropy(), metrics = ["accuracy"])
model.compile(optimizer=my_optimizer, loss=my_loss, metrics=["accuracy"])

# In[69]:

history = model.fit(x_train, y_train, epochs=my_epochs, verbose=1, shuffle=True, validation_split=0.2)

# In[80]:


history.history.keys()
# Out[88]: dict_keys(['val_loss', 'val_accuracy', 'loss', 'accuracy'])
history.history['accuracy']


# In[77]:


import matplotlib.pyplot as plt
#get_ipython().run_line_magic('matplotlib', 'inline')
#%%
# Plot training & validation accuracy values
plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
plt.title('Model_3 accuracy')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.savefig("Accuracy_model_3.png")
plt.show()
#%%
# Plot training & validation loss values
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('Model_3 loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.savefig("Loss_model_3.png")
plt.show()

#%%





# In[192]:




# I
#

# n[ ]

#%%
# MODEL 4
#%%
# Chenge kernal_weight_decay = 0.001
# activity_weight_decay = 0.001

#%% Define parameters
kernel_size = (row_n, 1)
n_filters = 32
x_train_shape = x_train.shape[1:4]
Conv2D_dim_1 = (1, 3)
Conv2D_n_1 = 64
pool_size_1 = (1,3)
Dense_n_1 = 64
my_optimizer = 'adam'
my_loss = 'binary_crossentropy'
kernal_weight_decay = 0.001
activity_weight_decay = 0.001

my_epochs = 16

# In[68]:


from keras.models import Sequential
from keras.layers import *
from tensorflow.keras import regularizers

model = Sequential()
model.add(
    Conv2D(filters=n_filters, kernel_size=kernel_size, strides=1, activation='relu', input_shape=x_train_shape, use_bias=True))
model.add(MaxPooling2D(pool_size=Conv2D_dim_1))
model.add(Flatten())

model.add(Dense(Dense_n_1, activation='relu',
             kernel_regularizer= regularizers.l2(kernal_weight_decay),
             activity_regularizer= regularizers.l1(activity_weight_decay)))
model.add(Dropout(0.25))

model.add(Dense(1, activation='sigmoid'))  # changed


# model.compile(optimizer='adam', loss='mse', metrics = ["accuracy"])
# model.compile(optimizer='sgd', loss=tf.keras.losses.BinaryCrossentropy(), metrics = ["accuracy"])
model.compile(optimizer=my_optimizer, loss=my_loss, metrics=["accuracy"])

# In[69]:

history = model.fit(x_train, y_train, epochs=my_epochs, verbose=1, shuffle=True, validation_split=0.2)

# In[80]:


history.history.keys()
# Out[88]: dict_keys(['val_loss', 'val_accuracy', 'loss', 'accuracy'])
history.history['accuracy']


# In[77]:


import matplotlib.pyplot as plt
#get_ipython().run_line_magic('matplotlib', 'inline')
#%%
# Plot training & validation accuracy values
plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
plt.title('Model_4 accuracy')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.savefig("Accuracy_model_4.png")
plt.show()
#%%
# Plot training & validation loss values
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('Model_4 loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.savefig("Loss_model_4.png")
plt.show()

#%%
from keras.callbacks import EarlyStopping
early_stopping = EarlyStopping(monitor='val_loss', patience=2)
#model.fit(x, y, validation_split=0.2, callbacks=[early_stopping])
history_norm = model.fit(x_train, y_train, callbacks=[early_stopping], epochs=15, verbose=1, shuffle = True, validation_split=0.2)



#%%
# MODEL 5
#%%
# Chenge kernal_weight_decay = 0.001
# activity_weight_decay = 0.001

#%% Define parameters
kernel_size = (row_n, 1)
n_filters = 8
x_train_shape = x_train.shape[1:4]
Conv2D_dim_1 = (1, 3)
Conv2D_n_1 = 16
pool_size_1 = (1,3)
Dense_n_1 = 64
my_optimizer = 'adam'
my_loss = 'binary_crossentropy'
kernal_weight_decay = 0.001
activity_weight_decay = 0.0001

my_epochs = 100

# In[68]:


from keras.models import Sequential
from keras.layers import *
from tensorflow.keras import regularizers

model = Sequential()
model.add(
    Conv2D(filters=n_filters, kernel_size=kernel_size, strides=1, activation='relu', input_shape=x_train_shape, use_bias=True))
model.add(MaxPooling2D(pool_size=Conv2D_dim_1))
model.add(Flatten())

model.add(Dense(Dense_n_1, activation='relu',
             kernel_regularizer= regularizers.l1(kernal_weight_decay),
             ))
model.add(Dropout(0.25))

model.add(Dense(1, activation='sigmoid'))  # changed


# model.compile(optimizer='adam', loss='mse', metrics = ["accuracy"])
# model.compile(optimizer='sgd', loss=tf.keras.losses.BinaryCrossentropy(), metrics = ["accuracy"])
model.compile(optimizer=my_optimizer, loss=my_loss, metrics=["accuracy"])

# In[69]:
#%%
from keras.callbacks import EarlyStopping
early_stopping = EarlyStopping(monitor='val_loss', patience=2)
#model.fit(x, y, validation_split=0.2, callbacks=[early_stopping])
history_norm = model.fit(x_train, y_train, callbacks=[early_stopping], epochs=15, verbose=1, shuffle = True, validation_split=0.2)
#%%

_, acc = model.evaluate(x_test, y_test, verbose=0)
print('Test Accuracy: %.3f' % (acc * 100))
#%%

history = model.fit(x_train, y_train, epochs=my_epochs, verbose=1, shuffle=True, validation_split=0.2)

# In[80]:


history.history.keys()
# Out[88]: dict_keys(['val_loss', 'val_accuracy', 'loss', 'accuracy'])
history.history['accuracy']


# In[77]:


import matplotlib.pyplot as plt
#get_ipython().run_line_magic('matplotlib', 'inline')
#%%
# Plot training & validation accuracy values
plt.plot(history_norm.history['accuracy'])
plt.plot(history_norm.history['val_accuracy'])
plt.title('Model_x accuracy')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
#plt.savefig("Accuracy_model_4.png")
plt.show()
#%%
# Plot training & validation loss values
plt.plot(history_norm.history['loss'])
plt.plot(history_norm.history['val_loss'])
plt.title('Model_X loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
#plt.savefig("Loss_model_4.png")
plt.show()

