#!/usr/bin/env python
# coding: utf-8

# In[1]:


from __future__ import absolute_import, division, print_function, unicode_literals
import functools

import tensorflow as tf


# In[2]:


import numpy as np
import pandas as pd
import glob #if you want to read files from global invironment


# In[3]:


import pandas as pd
from sklearn.model_selection import train_test_split
#from sklearn.datasets import load_iris


# In[4]:


#train_set_y_orig = np.array(pd.read_csv("../output_data/CNN_expression_output_mean_apex_inflorescence_21d_AGI.csv"))


# In[12]:


train_set_y_orig = pd.read_csv("../Deep_learning_arabidopsis/CNN_expression_output_mean_apex_inflorescence_21d_AGI.csv")


# In[26]:


type(train_set_y_orig)


# In[13]:


train_set_y_orig.shape


# In[14]:


train_set_y_orig.head(3)


# In[28]:


#test = train_set_y_orig.sample(n = 2000)
#np.array(test.AGI)

# set a seed for sample’s random number generator using the random_state argument
# train_set_y_orig.sample(n = 2000, random_state=2)


# In[30]:


file_names=[s + ".csv" for s in np.array(train_set_y_orig.AGI)]


# In[34]:


path = "../Deep_learning_arabidopsis/CNN_input_matrix_df/"

file_list = [path + s for s in file_names]


#file_list = (path + file_names)
print(file_list[0:5])


# In[35]:


dfs=np.array([pd.read_csv(fp).values for fp in file_list])


# In[48]:


dfs.shape


# In[122]:


# Define training set
Y_train = (np.array(train_set_y_orig['mean_apex inflorescence_21d'])).reshape(test['mean_apex inflorescence_21d'].shape[0], 1)
X_train = dfs.reshape((train_set_y_orig.shape[0]), 872, 1500, 1)


# In[123]:


print ("number of training examples = " + str(X_train.shape[0]))

print ("X_train shape: " + str(X_train.shape))
print ("Y_train shape: " + str(Y_train.shape))


# In[126]:


from keras.models import Sequential
from keras.layers import *

model=Sequential()
model.add(MaxPooling2D(pool_size=(1,100)))
model.add(Conv2D(filters=128, kernel_size=(872, 1), strides=1, activation='relu', input_shape=(872, 15, 1), use_bias=True))
model.add(MaxPooling2D(pool_size=(1,3)))
model.add(Flatten())
model.add(Dense(200, activation='relu'))
#
#model.add(Dense(200, activation='relu',
#              kernel_regularizer=regularizers.l2(0.01),
#              activity_regularizer=regularizers.l1(0.01)))
#model.add(Dense(20, activation='relu'))
#model.add(Dense(16, activation='relu'))
model.add(Dense(1, activation='sigmoid')) # changed
          
# model.add(Flatten())
# model.add(Dense(32*2, activation='relu'))
# model.add(Dense(32, activation='relu'))
# model.add(Dense(16, activation='relu'))
# model.add(Dense(1, activation='linear'))

         

model.compile(optimizer='adam', loss='mse', metrics = ["accuracy"])


# In[128]:


history = model.fit(X_train, Y_train, epochs=1, verbose=1, shuffle = True, validation_split=0.2)


# In[129]:


from keras.models import Sequential
from keras.layers import *

model=Sequential()
model.add(MaxPooling2D(pool_size=(1,100)))
model.add(Conv2D(filters=128, kernel_size=(872, 1), strides=1, activation='relu', input_shape=(872, 15, 1), use_bias=True))
model.add(MaxPooling2D(pool_size=(1,3)))
model.add(Flatten())
#
model.add(Dense(200, activation='relu',
              kernel_regularizer=regularizers.l2(0.01),
              activity_regularizer=regularizers.l1(0.01)))
#model.add(Dense(20, activation='relu'))
#model.add(Dense(16, activation='relu'))
model.add(Dense(1, activation='sigmoid')) # changed
          
# model.add(Flatten())
# model.add(Dense(32*2, activation='relu'))
# model.add(Dense(32, activation='relu'))
# model.add(Dense(16, activation='relu'))
# model.add(Dense(1, activation='linear'))

         

model.compile(optimizer='adam', loss='mse', metrics = ["accuracy"])


# In[130]:


history = model.fit(X_train, Y_train, epochs=25, verbose=1, shuffle = True, validation_split=0.2)


# In[131]:


X_train.shape


# In[132]:


from keras.models import Sequential
from keras.layers import *

model=Sequential()
model.add(MaxPooling2D(pool_size=(1,100)))
model.add(Conv2D(filters=128, kernel_size=(872, 1), strides=1, activation='relu', input_shape=(872, 15, 1), use_bias=True))
model.add(MaxPooling2D(pool_size=(1,3)))
model.add(Flatten())
model.add(Dense(100, activation='relu'))
#
#model.add(Dense(200, activation='relu',
#              kernel_regularizer=regularizers.l2(0.01),
#              activity_regularizer=regularizers.l1(0.01)))
#model.add(Dense(20, activation='relu'))
#model.add(Dense(16, activation='relu'))
model.add(Dense(1, activation='sigmoid')) # changed
          
# model.add(Flatten())
# model.add(Dense(32*2, activation='relu'))
# model.add(Dense(32, activation='relu'))
# model.add(Dense(16, activation='relu'))
# model.add(Dense(1, activation='linear'))

         

model.compile(optimizer='adam', loss='mse', metrics = ["accuracy"])


# In[ ]:


history = model.fit(X_train, Y_train, epochs=25, verbose=1, shuffle = True, validation_split=0.2)


# In[ ]:




