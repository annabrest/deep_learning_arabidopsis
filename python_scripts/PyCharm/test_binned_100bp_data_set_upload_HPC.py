#!/usr/bin/env python
# coding: utf-8

# In[43]:


from __future__ import absolute_import, division, print_function, unicode_literals
import functools

import tensorflow as tf


# In[45]:


import numpy as np
import pandas as pd
import glob #if you want to read files from global invironment


# In[46]:


import pandas as pd
from sklearn.model_selection import train_test_split


# In[295]:


train_set_y_orig = pd.read_csv("../output_data/CNN_expression_output_mean_apex_inflorescence_21d_AGI.csv")


# In[245]:


train_set_y_positive = pd.read_csv("../output_data/CNN_expression_output_positive_cntr.csv")


# In[296]:


type(train_set_y_orig)


# In[297]:


train_set_y_orig.shape


# In[298]:


train_set_y_positive.shape


# In[299]:


train_set_y_orig.head(3)


# In[300]:


train_set_y_positive.head(3)


# In[301]:



train_set_y_positive_subset = pd.merge(left=train_set_y_orig, right=train_set_y_positive, how='outer', left_on='AGI', right_on='AGI')


# In[302]:


print(train_set_y_positive_subset.shape)
train_set_y_positive_subset.head(3)


# In[304]:


train_set_y_positive_subset[ pd.isnull(train_set_y_positive_subset.mean_apex_inflorescence_21d) ]


# In[308]:


train_set_y_positive_subset[train_set_y_positive_subset.output == 0]


# In[ ]:





# In[ ]:





# In[51]:


#test = train_set_y_orig.sample(n = 2000)
#np.array(test.AGI)

# set a seed for sample’s random number generator using the random_state argument
# train_set_y_orig.sample(n = 2000, random_state=2)


# In[52]:


file_names=[s + ".csv" for s in np.array(train_set_y_orig.AGI)]


# In[53]:


path = "../output_data/CNN_input_matrix_df_bin_100bp/"

file_list = [path + s for s in file_names]


#file_list = (path + file_names)
print(file_list[0:5])


# In[54]:


dfs=np.array([pd.read_csv(fp).values for fp in file_list])


# In[55]:


import time
start_time = time.time()


dfs_fl64=np.array([pd.read_csv(fp).values for fp in file_list])

def main():
    print("--- %s seconds ---" % (time.time() - start_time))
    print("--- %.4f seconds ---" % (time.time() - start_time))
main()


# In[56]:


import time
start_time = time.time()


dfs_fl32=np.float32([pd.read_csv(fp).values for fp in file_list])

def main():
    print("--- %s seconds ---" % (time.time() - start_time))
    print("--- %.4f seconds ---" % (time.time() - start_time))
main()


# In[57]:


dfs_fl32.shape


# In[58]:


dfs.shape


# In[59]:


# Define training set
Y_train = (np.array(train_set_y_orig['mean_apex inflorescence_21d'])).reshape(train_set_y_orig['mean_apex inflorescence_21d'].shape[0], 1)
X_train = dfs.reshape((train_set_y_orig.shape[0]), 872, 15, 1)


# In[187]:


X_train_norm[1]


# In[67]:


print ("number of training examples = " + str(X_train.shape[0]))
print ("X_train shape: " + str(X_train.shape))
print ("Y_train shape: " + str(Y_train.shape))

#x_train, x_test, y_train, y_test = train_test_split(X_train, Y_train, test_size = 0.2)
#next time set the seed
x_train, x_test, y_train, y_test = train_test_split(X_train, Y_train, test_size = 0.2, random_state=11) #set the seed by "random_state=11"

print ("number of training examples = " + str(x_train.shape[0]))
print ("x_train shape: " + str(x_train.shape))
print ("y_train shape: " + str(y_train.shape))


# In[68]:


from keras.models import Sequential
from keras.layers import *

model=Sequential()
#model.add(MaxPooling2D(pool_size=(1,100)))
model.add(Conv2D(filters=128, kernel_size=(872, 1), strides=1, activation='relu', input_shape=(872, 15, 1), use_bias=True))
model.add(MaxPooling2D(pool_size=(1,3)))
model.add(Flatten())
model.add(Dense(200, activation='relu'))
#
#model.add(Dense(200, activation='relu',
#              kernel_regularizer=regularizers.l2(0.01),
#              activity_regularizer=regularizers.l1(0.01)))
#model.add(Dense(20, activation='relu'))
#model.add(Dense(16, activation='relu'))
model.add(Dense(1, activation='sigmoid')) # changed
          
# model.add(Flatten())
# model.add(Dense(32*2, activation='relu'))
# model.add(Dense(32, activation='relu'))
# model.add(Dense(16, activation='relu'))
# model.add(Dense(1, activation='linear'))

         

#model.compile(optimizer='adam', loss='mse', metrics = ["accuracy"])
#model.compile(optimizer='sgd', loss=tf.keras.losses.BinaryCrossentropy(), metrics = ["accuracy"])
model.compile(optimizer='adam', loss=tf.keras.losses.BinaryCrossentropy(), metrics = ["accuracy"])


# In[69]:


history = model.fit(x_train, y_train, epochs=1, verbose=1, shuffle = True, validation_split=0.2)


# In[70]:


history = model.fit(x_train, y_train, epochs=100, verbose=1, shuffle = True, validation_split=0.2)


# In[80]:


history.history


# In[ ]:





# In[77]:


import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')

# Plot training & validation accuracy values
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('Model accuracy')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.show()

# Plot training & validation loss values
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('Model loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.show()


# In[ ]:





# In[243]:


from keras.models import Sequential
from keras.layers import *

model=Sequential()
model.add(MaxPooling2D(pool_size=(1,5)))
model.add(Conv2D(filters=16, kernel_size=(872, 1), strides=1, activation='relu', input_shape=(872, 3, 1), use_bias=True))
#model.add(MaxPooling2D(pool_size=(1,3)))
model.add(Flatten())
#model.add(Dense(200, activation='relu'))
#
model.add(Dense(16, activation='relu',
              kernel_regularizer=regularizers.l1(0.0001),
              activity_regularizer=regularizers.l1(0.0001)))
#model.add(Dense(8, activation='relu',
#              kernel_regularizer=regularizers.l1(0.0001),
#              activity_regularizer=regularizers.l1(0.0001)))
#model.add(Dense(20, activation='relu'))
#model.add(Dense(16, activation='relu'))
model.add(Dense(1, activation='sigmoid')) # changed
          
# model.add(Flatten())
# model.add(Dense(32*2, activation='relu'))
# model.add(Dense(32, activation='relu'))
# model.add(Dense(16, activation='relu'))
# model.add(Dense(1, activation='linear'))

         

#model.compile(optimizer='adam', loss='mse', metrics = ["accuracy"])
#model.compile(optimizer='sgd', loss=tf.keras.losses.BinaryCrossentropy(), metrics = ["accuracy"])
model.compile(optimizer='adam', loss=tf.keras.losses.BinaryCrossentropy(), metrics = ["accuracy"])


# In[244]:


history_norm = model.fit(x_train_norm, y_train, epochs=15, verbose=1, shuffle = True, validation_split=0.2)


# In[240]:


from keras.callbacks import EarlyStopping
early_stopping = EarlyStopping(monitor='val_loss', patience=2)
#model.fit(x, y, validation_split=0.2, callbacks=[early_stopping])
history_norm = model.fit(x_train, y_train, callbacks=[early_stopping], epochs=15, verbose=1, shuffle = True, validation_split=0.2)


# In[ ]:





# In[ ]:


#Try simple normalisation


# In[ ]:





# In[185]:


X_train_norm = X_train/100


# In[188]:


print ("number of training examples = " + str(X_train.shape[0]))
print ("X_train shape: " + str(X_train.shape))
print ("Y_train shape: " + str(Y_train.shape))

#next time set the seed
x_train_norm, x_test_norm, y_train, y_test = train_test_split(X_train_norm, Y_train, test_size = 0.2, random_state=11) #set the seed by "random_state=11"

print ("number of training examples norm = " + str(x_train_norm.shape[0]))
print ("x_train_norm shape: " + str(x_train_norm.shape))
print ("y_train shape: " + str(y_train.shape))


# In[189]:


from keras.models import Sequential
from keras.layers import *

model=Sequential()
#model.add(MaxPooling2D(pool_size=(1,100)))
model.add(Conv2D(filters=128, kernel_size=(872, 1), strides=1, activation='relu', input_shape=(872, 15, 1), use_bias=True))
model.add(MaxPooling2D(pool_size=(1,3)))
model.add(Flatten())
model.add(Dense(200, activation='relu'))
#
#model.add(Dense(200, activation='relu',
#              kernel_regularizer=regularizers.l2(0.01),
#              activity_regularizer=regularizers.l1(0.01)))
#model.add(Dense(20, activation='relu'))
#model.add(Dense(16, activation='relu'))
model.add(Dense(1, activation='sigmoid')) # changed
          
# model.add(Flatten())
# model.add(Dense(32*2, activation='relu'))
# model.add(Dense(32, activation='relu'))
# model.add(Dense(16, activation='relu'))
# model.add(Dense(1, activation='linear'))

         

#model.compile(optimizer='adam', loss='mse', metrics = ["accuracy"])
#model.compile(optimizer='sgd', loss=tf.keras.losses.BinaryCrossentropy(), metrics = ["accuracy"])
model.compile(optimizer='adam', loss=tf.keras.losses.BinaryCrossentropy(), metrics = ["accuracy"])


# In[191]:


history_norm = model.fit(x_train_norm, y_train, epochs=15, verbose=1, shuffle = True, validation_split=0.2)


# In[ ]:





# In[192]:


from keras.callbacks import EarlyStopping
early_stopping = EarlyStopping(monitor='val_loss', patience=2)
#model.fit(x, y, validation_split=0.2, callbacks=[early_stopping])
history_norm = model.fit(x_train_norm, y_train, callbacks=[early_stopping], epochs=15, verbose=1, shuffle = True, validation_split=0.2)


# In[ ]:





# In[193]:


from keras.models import Sequential
from keras.layers import *

model=Sequential()
model.add(Conv2D(filters=128, kernel_size=(872, 1), strides=1, activation='relu', input_shape=(872, 15, 1), use_bias=True))
model.add(MaxPooling2D(pool_size=(1,3)))
model.add(Flatten())
model.add(Dense(64, activation='relu'))
#
#model.add(Dense(200, activation='relu',
#              kernel_regularizer=regularizers.l2(0.01),
#              activity_regularizer=regularizers.l1(0.01)))
model.add(Dense(32, activation='relu'))
model.add(Dense(16, activation='relu'))
model.add(Dense(1, activation='sigmoid')) # changed
          

#model.compile(optimizer='adam', loss='mse', metrics = ["accuracy"])
model.compile(optimizer='sgd', loss=tf.keras.losses.BinaryCrossentropy(), metrics = ["accuracy"])
#model.compile(optimizer='adam', loss=tf.keras.losses.BinaryCrossentropy(), metrics = ["accuracy"])


# In[194]:


from keras.callbacks import EarlyStopping
early_stopping = EarlyStopping(monitor='val_loss', patience=2)
history_norm = model.fit(x_train_norm, y_train, callbacks=[early_stopping], epochs=15, verbose=1, shuffle = True, validation_split=0.2)


# In[ ]:





# In[202]:


from keras.models import Sequential
from keras.layers import *

model=Sequential()
model.add(Conv2D(filters=128, kernel_size=(872, 1), strides=1, activation='relu', input_shape=(872, 15, 1), use_bias=True))
model.add(MaxPooling2D(pool_size=(1,3)))
model.add(Flatten())
model.add(Dense(200, activation='relu'))
#
#model.add(Dense(200, activation='relu',
#              kernel_regularizer=regularizers.l2(0.01),
#              activity_regularizer=regularizers.l1(0.01)))
#model.add(Dense(32, activation='relu'))
#model.add(Dense(16, activation='relu'))
#model.add(Dense(1, activation='sigmoid')) # changed
          

#model.compile(optimizer='adam', loss='mse', metrics = ["accuracy"])
#model.compile(optimizer='sgd', loss=tf.keras.losses.BinaryCrossentropy(), metrics = ["accuracy"])
model.compile(optimizer='adam', loss=tf.keras.losses.BinaryCrossentropy(), metrics = ["accuracy"])


# In[203]:


from keras.callbacks import EarlyStopping
early_stopping = EarlyStopping(monitor='val_loss', patience=2)
history_norm = model.fit(x_train, y_train, callbacks=[early_stopping], epochs=15, verbose=1, shuffle = True, validation_split=0.2)


# In[ ]:





# In[206]:


from keras.models import Sequential
from keras.layers import *

model=Sequential()
#model.add(MaxPooling2D(pool_size=(1,100)))
model.add(Conv2D(filters=128, kernel_size=(872, 1), strides=1, activation='relu', input_shape=(872, 15, 1), use_bias=True))
model.add(MaxPooling2D(pool_size=(1,3)))
model.add(Flatten())
model.add(Dense(200, activation='relu'))
#
#model.add(Dense(200, activation='relu',
#              kernel_regularizer=regularizers.l2(0.01),
#              activity_regularizer=regularizers.l1(0.01)))
#model.add(Dense(20, activation='relu'))
#model.add(Dense(16, activation='relu'))
model.add(Dense(1, activation='sigmoid')) # changed
          
# model.add(Flatten())
# model.add(Dense(32*2, activation='relu'))
# model.add(Dense(32, activation='relu'))
# model.add(Dense(16, activation='relu'))
# model.add(Dense(1, activation='linear'))

         

#model.compile(optimizer='adam', loss='mse', metrics = ["accuracy"])
#model.compile(optimizer='sgd', loss=tf.keras.losses.BinaryCrossentropy(), metrics = ["accuracy"])
model.compile(optimizer='adam', loss=tf.keras.losses.BinaryCrossentropy(), metrics = ["accuracy"])


# In[207]:


history_norm = model.fit(x_train_norm, y_train, epochs=15, verbose=1, shuffle = True, validation_split=0.2)


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:


# Keras supports the early stopping of training via a callback called EarlyStopping


# # Try to normalise data

# In[155]:


from sklearn import preprocessing

dfs.shape

x_scaled = np.array([preprocessing.scale(fp) for fp in dfs])


# In[167]:


pd.DataFrame([x.mean(axis = 1) for x in x_scaled])


# In[166]:


test_2 = np.array([[ 1., -1.],
                   [ 2.,  0.],
                   [ 0.,  1.]])
print(test_2.shape)
test_2_sc = preprocessing.scale(test_2)
test_2_sc.mean(axis=0)


# In[184]:


dfs[0:1]/100

test = dfs.reshape((train_set_y_orig.shape[0]), 15, 872)
print(test.shape)
#test[1]


test_sc = preprocessing.scale(test[1])
test_sc.mean(axis=0).shape


# In[156]:


print(x_scaled.mean(axis = 0))#Why not "zeros"?
print(x_scaled.std(axis = 0))
print(x_scaled[1].mean(axis = 0))#Why not "zeros"?even very low
print(x_scaled[1].std(axis = 0))


# In[129]:


x_scaled.shape


# In[149]:


test_1 = preprocessing.scale(dfs[1])


# In[152]:


print(type(dfs[1].shape))
print(test_1.mean(axis = 0))#Why not "zeros"?even very low
print(test_1.std(axis = 0))


# In[151]:


test_1.shape


# In[64]:


from keras.models import Sequential
from keras.layers import *

model=Sequential()
#model.add(experimental.preprocessing.Rescaling(1.0 / 100))
model.add(Conv2D(filters=128, kernel_size=(872, 1), strides=1, activation='relu', input_shape=(872, 15, 1), use_bias=True))
model.add(MaxPooling2D(pool_size=(1,3)))
model.add(Flatten())
#model.add(Dense(200, activation='relu'))
#
model.add(Dense(200, activation='relu',
              kernel_regularizer=regularizers.l2(0.01),
              activity_regularizer=regularizers.l1(0.01)))
#model.add(Dense(20, activation='relu'))
#model.add(Dense(16, activation='relu'))
model.add(Dense(1, activation='sigmoid')) # changed
          
# model.add(Flatten())
# model.add(Dense(32*2, activation='relu'))
# model.add(Dense(32, activation='relu'))
# model.add(Dense(16, activation='relu'))
# model.add(Dense(1, activation='linear'))

         

#model.compile(optimizer='adam', loss='mse', metrics = ["accuracy"])
model.compile(optimizer='adam', loss=tf.keras.losses.BinaryCrossentropy(), metrics = ["accuracy"])
model.summary()


# In[65]:


history = model.fit(x_train, y_train, epochs=100, verbose=1, shuffle = True, validation_split=0.2)


# In[ ]:




