#!/usr/bin/env python
# coding: utf-8

# In[1]:
# Load packages:

from __future__ import absolute_import, division, print_function, unicode_literals
import functools
import tensorflow as tf
import numpy as np
import pandas as pd
import glob #if you want to read files from global invironment
from sklearn.model_selection import train_test_split


# In[2]:
# Read file with labels
path_to_output_data = "/Users/abrestovitsky/Work/2019/Analysis/Deep_learning_arabidopsis/output_data/"
# file_name_labels = "CNN_expression_output_mean_apex_inflorescence_21d_AGI.csv"
file_name_labels = "CNN_expression_output_mean_apex_inflorescence_21d_AGI_filtered_8to12TPM.csv"

train_set_y_orig = pd.read_csv(path_to_output_data + file_name_labels)
type(train_set_y_orig)
# Out[21]: pandas.core.frame.DataFrame

# In[3]:

print(train_set_y_orig.shape)
print(train_set_y_orig.head(3))
# (19987, 2)
#          AGI  mean_apex_inflorescence_21d
# 0  AT1G01010                            0
# 1  AT1G01020                            1
# 2  AT1G01030                            0



# In[4]:
# Read Input files

file_names=[s + ".csv" for s in np.array(train_set_y_orig.AGI)]


# In[5]:


folder_name_input = "CNN_input_matrix_df_bin_100bp/"

file_list = [path_to_output_data + folder_name_input + s for s in file_names]


#file_list = (path + file_names)
print(file_list[0:5])
# ['/Users/abrestovitsky/Work/2019/Analysis/Deep_learning_arabidopsis/output_data/CNN_input_matrix_df_bin_100bp/AT1G01010.csv',
# '/Users/abrestovitsky/Work/2019/Analysis/Deep_learning_arabidopsis/output_data/CNN_input_matrix_df_bin_100bp/AT1G01020.csv',
# '/Users/abrestovitsky/Work/2019/Analysis/Deep_learning_arabidopsis/output_data/CNN_input_matrix_df_bin_100bp/AT1G01030.csv',
# '/Users/abrestovitsky/Work/2019/Analysis/Deep_learning_arabidopsis/output_data/CNN_input_matrix_df_bin_100bp/AT1G01060.csv',
# '/Users/abrestovitsky/Work/2019/Analysis/Deep_learning_arabidopsis/output_data/CNN_input_matrix_df_bin_100bp/AT1G01070.csv']

# In[6]:


dfs=np.array([pd.read_csv(fp).values for fp in file_list])
print(dfs.shape)
# 19987, 872, 15)


# In[83]:
# Filter out TFs bound to more than 15000 genes
# Based on the calculation that have been done using R
Index_file = "CNN_input_matrix_df/TF_filtered_200to10000_index.csv"
TF_index = pd.read_csv(path_to_output_data + Index_file)
TF_index.columns = ["index"]

#%%
print(type(TF_index))
print(TF_index.head(3))
print(dfs[:,TF_index.index,:].shape)
# <class 'pandas.core.frame.DataFrame'>
#    index
# 0    248
# 1     53
# 2     63
# (19987, 755, 15)

#%% Update dfs to include info only for filtered TFs, based on index

dfs_filtered = dfs[:,TF_index.index,:]
dfs_filtered[1]


#%%---TO TRY!!!!
# Normalise data by devision by 100

#%% Normalise by dividing by 100

dfs_filtered_norm = dfs_filtered/100

print('not normalised min=%.3f, max=%.3f' % (dfs_filtered.min(), dfs_filtered.max()))
print('normalised min=%.3f, max=%.3f' % (dfs_filtered_norm.min(), dfs_filtered_norm.max()))
# normalised min=0.000, max=99.996
# normalised min=0.000, max=1.000

#%%


row_n = dfs_filtered.shape[1]
col_n = dfs_filtered.shape[2]

# Define training set
Y_train = (np.array(train_set_y_orig['mean_apex_inflorescence_21d'])).reshape(train_set_y_orig['mean_apex_inflorescence_21d'].shape[0], 1)
X_train = dfs_filtered.reshape((train_set_y_orig.shape[0]), row_n, col_n, 1)
X_train_norm = dfs_filtered_norm.reshape((train_set_y_orig.shape[0]), row_n, col_n, 1)


# %%

print ("number of training examples = " + str(X_train.shape[0]))
print ("X_train shape: " + str(X_train.shape))
print ("Y_train shape: " + str(Y_train.shape))
# number of training examples = 19987
# X_train shape: (19987, 755, 15, 1)
# Y_train shape: (19987, 1)

print ("number of training examples after normalisation = " + str(X_train.shape[0]))
print ("X_train_norm shape: " + str(X_train_norm.shape))

# number of training examples after normalisation = 19987
# X_train_norm shape: (19987, 755, 15, 1)


#%% Split data set into "train" and "test":
# set the seed by "random_state=11"

#x_train, x_test, y_train, y_test = train_test_split(X_train, Y_train, test_size = 0.2)
#next time set the seed

x_train, x_test, y_train, y_test = train_test_split(X_train, Y_train, test_size = 0.2, random_state=11) #set the seed by "random_state=11"

# acheck new dim of the data set after split:
print ("number of training examples = " + str(x_train.shape[0]))
print ("x_train shape: " + str(x_train.shape))
print ("y_train shape: " + str(y_train.shape))

# number of training examples = 15989
# x_train shape: (15989, 755, 15, 1)
# y_train shape: (15989, 1)
####
#%% Split after normalisation:

x_train_norm, x_test_norm, y_train, y_test = train_test_split(X_train_norm, Y_train, test_size = 0.2, random_state=11) #set the seed by "random_state=11"

# acheck new dim of the data set after split:
print ("number of training examples after normalisation = " + str(x_train_norm.shape[0]))
print ("x_train_norm shape: " + str(x_train_norm.shape))
print ("x_test_norm shape: " + str(x_test_norm.shape))
print ("y_train shape: " + str(y_train.shape))

# number of training examples after normalisation = 15989
# x_train_norm shape: (15989, 755, 15, 1)
# x_test_norm shape: (3998, 755, 15, 1)
# y_train shape: (15989, 1)

#%%

#%%

#%%

#%%

from keras.utils import to_categorical
from keras.models import Sequential
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Dense
from keras.layers import Flatten
from keras.preprocessing.image import ImageDataGenerator

from tensorflow import keras
#from tensorflow.keras import layers


#%%
# *MODEL 2*
#%%
#%% Define parameters
kernel_size = (row_n, 1)
n_filters = 32
x_train_shape = x_train.shape[1:4]
Conv2D_dim_1 = (1, 3)
Conv2D_n_1 = 64
pool_size_1 = (1,3)
Dense_n_1 = 64
# my_optimizer = keras.optimizers.Adam(learning_rate=0.0001)

lr_schedule = keras.optimizers.schedules.ExponentialDecay(
    initial_learning_rate=1e-2,
    decay_steps=10000,
    decay_rate=0.9)
my_optimizer = keras.optimizers.SGD(learning_rate=lr_schedule)


my_loss = 'binary_crossentropy'
# tf.keras.losses.BinaryCrossentropy()
my_epochs = 16
 # In[68]:


from keras.models import Sequential
from keras.layers import *

model_2 = Sequential()
model_2.add(
    Conv2D(filters=n_filters, kernel_size=kernel_size, strides=1, activation='relu', input_shape=x_train_shape, use_bias=True))
model_2.add(MaxPooling2D(pool_size=Conv2D_dim_1))
model_2.add(Flatten())
model_2.add(Dense(Dense_n_1, activation='relu'))

model_2.add(Dense(1, activation='sigmoid'))  # changed


# model.compile(optimizer='adam', loss='mse', metrics = ["accuracy"])
# model.compile(optimizer='sgd', loss=tf.keras.losses.BinaryCrossentropy(), metrics = ["accuracy"])
model_2.compile(optimizer=my_optimizer, loss=my_loss, metrics=["accuracy"])

#%%
norm_histories = {}
norm_histories['no_norm'] = model_2.fit(x_train, y_train, epochs=my_epochs, verbose=1, shuffle=True, validation_split=0.2)
norm_histories['norm'] = model_2.fit(x_train_norm, y_train, epochs=my_epochs, verbose=1, shuffle=True, validation_split=0.2)
# 12791/12791 [==============================] - 2s 172us/step - loss: 0.1899 - accuracy: 0.9267 - val_loss: 1.6425 - val_accuracy: 0.5094
# 12791/12791 [==============================] - 3s 202us/step - loss: 0.5874 - accuracy: 0.7100 - val_loss: 0.6958 - val_accuracy: 0.5453

#%%
import matplotlib.pyplot as plt
#%%
# Plot training & validation accuracy values for normalised and non-normalised data
plt.plot(norm_histories['no_norm'].history['accuracy'])
plt.plot(norm_histories['no_norm'].history['val_accuracy'])
plt.plot(norm_histories['norm'].history['accuracy'])
plt.plot(norm_histories['norm'].history['val_accuracy'])

plt.title('Model_2_filtered_norm accuracy')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train_no_norm', 'Test_no_norm', 'Train_norm', 'Test_norm'], loc='upper left')
plt.savefig("plots_PyCharm/Accuracy_model_2_filtered_norm.png")
plt.show()

#%%
norm_histories['no_norm'].history['accuracy']
norm_histories['norm'].history
#%%
type(norm_histories)
norm_histories.values()
# %%
plt.plot(norm_histories['no_norm'].history['loss'])
plt.plot(norm_histories['no_norm'].history['val_loss'])
plt.plot(norm_histories['norm'].history['loss'])
plt.plot(norm_histories['norm'].history['val_loss'])

plt.title('Model_2_filtered_norm loss')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train_no_norm', 'Test_no_norm', 'Train_norm', 'Test_norm'], loc='upper left')
plt.savefig("plots_PyCharm/Loss_model_2_filtered_norm.png")
plt.show()


#%%

#########################

#########################

#%% # Compile differently

model_2.compile(optimizer=my_optimizer,
                loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
                metrics=[
                  tf.keras.losses.BinaryCrossentropy(
                      from_logits=True, name='binary_crossentropy'),
                  'accuracy'])

#%%
#%%
my_epochs = 100
# history = model.fit(x_train_norm, y_train, epochs=my_epochs, verbose=1, shuffle=True, validation_split=0.2)
#
from keras.callbacks import EarlyStopping

# early_stopping = EarlyStopping(monitor='val_loss', patience=2)
early_stopping = EarlyStopping(monitor='val_binary_crossentropy', patience=200)

# model.fit(x, y, validation_split=0.2, callbacks=[early_stopping])
history_norm_early_stoping = model_2.fit(x_train_norm, y_train, callbacks=[early_stopping], epochs=my_epochs, verbose=1, shuffle=True,
                                  validation_split=0.2)

norm_histories_epo100 = {}
norm_histories_epo100['no_norm'] = model_2.fit(x_train, y_train, callbacks=[early_stopping],
                                               epochs=my_epochs, verbose=1, shuffle=True, validation_split = 0.2)
norm_histories_epo100['norm'] = model_2.fit(x_train_norm, y_train, callbacks=[early_stopping],
                                               epochs=my_epochs, verbose=1, shuffle=True, validation_split = 0.2)



#%%
#############################

norm_histories_epo100['norm'].history.keys()
# dict_keys(['val_loss', 'val_binary_crossentropy', 'val_accuracy',
# 'loss', 'binary_crossentropy', 'accuracy'])

# %%


import matplotlib.pyplot as plt
#get_ipython().run_line_magic('matplotlib', 'inline')
#%%
# Plot training & validation accuracy values
#%%
# Plot training & validation accuracy values for normalised and non-normalised data
plt.plot(norm_histories_epo100['no_norm'].history['accuracy'])
plt.plot(norm_histories_epo100['no_norm'].history['val_accuracy'])
plt.plot(norm_histories_epo100['norm'].history['accuracy'])
plt.plot(norm_histories_epo100['norm'].history['val_accuracy'])

plt.title('Model_2_epo100_filtered_norm accuracy')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train_no_norm', 'Test_no_norm', 'Train_norm', 'Test_norm'], loc='upper left')
#plt.savefig("plots_PyCharm/Accuracy_model_2_epo100_filtered_norm.png")
plt.show()

#%%
# Plot training & validation loss values_
plt.plot(norm_histories_epo100['no_norm'].history['loss'])
plt.plot(norm_histories_epo100['no_norm'].history['val_loss'])
plt.plot(norm_histories_epo100['norm'].history['loss'])
plt.plot(norm_histories_epo100['norm'].history['val_loss'])

plt.title('Model_2_epo100_filtered_norm loss')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train_no_norm', 'Test_no_norm', 'Train_norm', 'Test_norm'], loc='upper left')
#plt.savefig("plots_PyCharm/Loss_model_2_epo100_filtered_norm.png")
plt.show()


#%%
#%%
# Plot training & validation loss values_
plt.plot(norm_histories_epo100['no_norm'].history['binary_crossentropy'])
plt.plot(norm_histories_epo100['no_norm'].history['val_binary_crossentropy'])
plt.plot(norm_histories_epo100['norm'].history['binary_crossentropy'])
plt.plot(norm_histories_epo100['norm'].history['val_binary_crossentropy'])

plt.title('Model_2_epo100_filtered_norm binary_crossentropy')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train_no_norm', 'Test_no_norm', 'Train_norm', 'Test_norm'], loc='upper left')
#plt.savefig("plots_PyCharm/Loss_model_2_epo100_filtered_norm.png")
plt.show()
#%%
#%%
# Evaluate the model on the test data using `evaluate`
print("Evaluate on test data")
results_model_2 = {}
results_model_2['non_norm'] = model_2.evaluate(x_test, y_test, batch_size=128)
results_model_2['norm'] = model_2.evaluate(x_test_norm, y_test, batch_size=128)
print("test loss, test acc:", 'non_norm:', results_model_2['non_norm'] ,'norm', results_model_2['norm'])
# test loss, test acc: non_norm: [0.7372778788872395, 0.7364626526832581, 0.5547773838043213]
# norm [0.7056461292663295, 0.705626368522644, 0.5600300431251526]
#%%

model_2.summary()
# Model: "sequential_4"
# _________________________________________________________________
# Layer (type)                 Output Shape              Param #
# =================================================================
# conv2d_3 (Conv2D)            (None, 1, 15, 32)         24192
# _________________________________________________________________
# max_pooling2d_3 (MaxPooling2 (None, 1, 5, 32)          0
# _________________________________________________________________
# flatten_3 (Flatten)          (None, 160)               0
# _________________________________________________________________
# dense_5 (Dense)              (None, 64)                10304
# _________________________________________________________________
# dense_6 (Dense)              (None, 1)                 65
# =================================================================
# Total params: 34,561
# Trainable params: 34,561
# Non-trainable params: 0
# _________________________________________________________________

#%%



#############
#%%
#%% *Model 3*
#%%
#%% Define parameters
kernel_size = (row_n, 1)
n_filters = 32
x_train_shape = x_train.shape[1:4]
Conv2D_dim_1 = (1, 3)
Conv2D_n_1 = 64
pool_size_1 = (1,3)
Dense_n_1 = 64
# my_optimizer = keras.optimizers.Adam(learning_rate=0.0001)

lr_schedule = keras.optimizers.schedules.ExponentialDecay(
    initial_learning_rate=1e-2,
    decay_steps=10000,
    decay_rate=0.9)
my_optimizer = keras.optimizers.SGD(learning_rate=lr_schedule)


my_loss = 'binary_crossentropy'
# tf.keras.losses.BinaryCrossentropy()
my_epochs = 100

 # %%

# from keras.models import Sequential
# from keras.layers import *

model_3 = Sequential()
model_3.add(
    Conv2D(filters=n_filters, kernel_size=kernel_size, strides=1, activation='relu', input_shape=x_train_shape, use_bias=True))
model_3.add(MaxPooling2D(pool_size=Conv2D_dim_1))
model_3.add(Flatten())
# fully connected layer
model_3.add(Dense(Dense_n_1, activation='relu'))
# add a batch normalization layer
model_3.add(BatchNormalization())
#model_3.add(Dropout(0.25))
# make predictions
model_3.add(Dense(1, activation='sigmoid'))


#%%
model_3.compile(optimizer=my_optimizer, loss=my_loss, metrics=["accuracy"])

#%%

# %%

#%%
my_epochs = 100

from keras.callbacks import EarlyStopping

# use early stopping to optimally terminate training through callbacks
# early_stopping = EarlyStopping(monitor='val_loss', patience=2)
# early_stopping = EarlyStopping(monitor='val_loss', patience=200)
early_stopping = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=100)


norm_histories_model_3 = {}
# norm_histories_model_3['no_norm'] = model_2.fit(x_train, y_train, callbacks=[early_stopping],
#                                                epochs=my_epochs, verbose=1, shuffle=True, validation_split = 0.2)
norm_histories_model_3['norm_BatchNormalization'] = model_3.fit(x_train_norm, y_train, callbacks=[early_stopping],
                                               epochs=my_epochs, verbose=1, shuffle=True, validation_split = 0.2)
# 2791/12791 [==============================] - 3s 200us/step - loss: 0.0067 - accuracy: 0.9988 - val_loss: 4.1490 - val_accuracy: 0.5457
# %%
# %%

#%%
# Plot training & validation loss values_
plt.plot(norm_histories_model_3['norm_BatchNormalization'].history['loss'])
plt.plot(norm_histories_model_3['norm_BatchNormalization'].history['val_loss'])

plt.title('Model_3_norm_BatchNormalization loss')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.savefig("plots_PyCharm/Loss_model_3_BatchNormalization_filtered_norm.png")
plt.show()


#%%
#%%
# Plot training & validation loss values_
plt.plot(norm_histories_model_3['norm_BatchNormalization'].history['accuracy'])
plt.plot(norm_histories_model_3['norm_BatchNormalization'].history['val_accuracy'])

plt.title('Model_3_norm_BatchNormalization accuracy')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.savefig("plots_PyCharm/accuracy_model_3_BatchNormalization_filtered_norm.png")
plt.show()
#%%
#########################
#########################
# %%
#############
#%%
#%% *Model 4*
#%%
#%% Define parameters
kernel_size = (row_n, 1)
n_filters = 32
x_train_shape = (755, 3, 1)
Conv2D_dim_1 = (1, 5)
Conv2D_dim_2 = (1, 3)
Conv2D_n_1 = 64
pool_size_1 = (1,5)
Dense_n_1 = 64
my_optimizer = keras.optimizers.Adam(learning_rate=0.0001)

# lr_schedule = keras.optimizers.schedules.ExponentialDecay(
#     initial_learning_rate=1e-2,
#     decay_steps=10000,
#     decay_rate=0.9)
# my_optimizer = keras.optimizers.SGD(learning_rate=lr_schedule)


my_loss = 'binary_crossentropy'
# tf.keras.losses.BinaryCrossentropy()
my_epochs = 100

 # %%

# from keras.models import Sequential
# from keras.layers import *

model_4 = Sequential()
model_4.add(MaxPooling2D(pool_size=Conv2D_dim_1)) # (1,5)
model_4.add(
    Conv2D(filters=n_filters, kernel_size=kernel_size, strides=1, activation='relu', input_shape=x_train_shape, use_bias=True))
# model_4.add(MaxPooling2D(pool_size=Conv2D_dim_2))
model_4.add(Flatten())
# fully connected layer
model_4.add(Dense(Dense_n_1, activation='relu'))
# add a batch normalization layer
model_4.add(BatchNormalization())
# model_4.add(Dropout(0.25))
# make predictions
model_4.add(Dense(1, activation='sigmoid'))


#%%
model_4.compile(optimizer=my_optimizer, loss=my_loss, metrics=["accuracy"])

#%%

# %%

#%%
my_epochs = 100

from keras.callbacks import EarlyStopping

early_stopping = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=10)


norm_histories_model_4 = {}
norm_histories_model_4['norm_BatchNormalization'] = model_4.fit(x_train_norm, y_train, callbacks=[early_stopping],
                                               epochs=my_epochs, verbose=1, shuffle=True, validation_split = 0.2)
# 12791/12791 [==============================] - 4s 304us/step - loss: 0.4604 - accuracy: 0.7766
# - val_loss: 0.8637 - val_accuracy: 0.5629
# Epoch 00011: early stopping
# %%
# %%
#%%
# Evaluate the model on the test data using `evaluate`
print("Evaluate on test data")
results_model_4 = model_4.evaluate(x_test_norm, y_test, batch_size=128)
print("test loss, test acc:", results_model_4)
# test loss, test acc: [0.8799670602513171, 0.5435217618942261]

#%%
# Plot training & validation loss values_
plt.plot(norm_histories_model_4['norm_BatchNormalization'].history['loss'])
plt.plot(norm_histories_model_4['norm_BatchNormalization'].history['val_loss'])

plt.title('Model_4_norm_BatchNormalization loss')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
#plt.savefig("plots_PyCharm/Loss_model_4_BatchNormalization_filtered_norm.png")
plt.show()


#%%
#%%
# Plot training & validation loss values_
plt.plot(norm_histories_model_4['norm_BatchNormalization'].history['accuracy'])
plt.plot(norm_histories_model_4['norm_BatchNormalization'].history['val_accuracy'])

plt.title('Model_4_norm_BatchNormalization accuracy')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
#plt.savefig("plots_PyCharm/accuracy_model_4_BatchNormalization_filtered_norm.png")
plt.show()
#%%
#############
#############

#%%
# MODEL 5
#%%
# Chenge kernal_weight_decay = 0.001
# activity_weight_decay = 0.001

#%% Define parameters
kernel_size = (row_n, 1)
n_filters = 32
x_train_shape = x_train.shape[1:4]
Conv2D_dim_1 = (1, 3)
Conv2D_n_1 = 64
pool_size_1 = (1,3)
Dense_n_1 = 64
my_optimizer = 'adam'
my_loss = 'binary_crossentropy'
kernal_weight_decay = 0.005
activity_weight_decay = 0.005

my_epochs = 100

# In[68]:


from keras.models import Sequential
from keras.layers import *
from tensorflow.keras import regularizers

model_5 = Sequential()
model_5.add(
    Conv2D(filters=n_filters, kernel_size=kernel_size, strides=1, activation='relu', input_shape=x_train_shape, use_bias=True))
model_5.add(MaxPooling2D(pool_size=Conv2D_dim_1))
model_5.add(Flatten())

model_5.add(Dense(Dense_n_1, activation='relu',
             kernel_regularizer= regularizers.l1(kernal_weight_decay),
             activity_regularizer= regularizers.l1(activity_weight_decay)))
# add a batch normalization layer
model_5.add(BatchNormalization())
model_5.add(Dropout(0.25))

model_5.add(Dense(1, activation='sigmoid'))  # changed

#%%
model_5.compile(optimizer=my_optimizer, loss=my_loss, metrics=["accuracy"])

#%%
###############
###############
#%%

from keras.callbacks import EarlyStopping

early_stopping = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=10)

norm_histories_model_5 = model_5.fit(x_train_norm, y_train, callbacks=[early_stopping],
                                               epochs=my_epochs, verbose=1, shuffle=True, validation_split = 0.2)
# kernal_weight_decay = 0.001
# activity_weight_decay = 0.001
# 12791/12791 [==============================] - 3s 217us/step - loss: 0.3070 - accuracy: 0.8977 - val_loss: 1.3208 - val_accuracy: 0.5507
# Epoch 00013: early stopping
# %%
# kernal_weight_decay = 0.01
# activity_weight_decay = 0.01
# 12791/12791 [==============================] - 4s 331us/step - loss: 0.7053 - accuracy: 0.5204 - val_loss: 0.7062 - val_accuracy: 0.5100
# Epoch 00014: early stopping
# %%
# kernal_weight_decay = 0.005
# activity_weight_decay = 0.005
# 12791/12791 [==============================] - 4s 313us/step - loss: 0.5518 - accuracy: 0.7602 - val_loss: 0.8754 - val_accuracy: 0.5428
# Epoch 00012: early stopping
# %%
#%%
# Evaluate the model on the test data using `evaluate`
print("Evaluate on test data")
results_model_5 = model_5.evaluate(x_test_norm, y_test)
print("test loss, test acc:", results_model_5)
# # kernal_weight_decay = 0.001
# # activity_weight_decay = 0.001
# test loss, test acc: [1.3618241137954936, 0.5510255098342896]
# kernal_weight_decay = 0.01
# activity_weight_decay = 0.01
# test loss, test acc: [0.7059781817032612, 0.5127564072608948]
# %%
# kernal_weight_decay = 0.005
# # activity_weight_decay = 0.005
# test loss, test acc: [0.8618459577796578, 0.5522761344909668]
#%%
# Plot training & validation loss values_
plt.plot(norm_histories_model_5.history['loss'])
plt.plot(norm_histories_model_5.history['val_loss'])

plt.title('Model_5_norm_BatchNormalization_Regul_3 loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.savefig("plots_PyCharm/Loss_model_5_3_BatchNormalization_filtered_norm.png")
plt.show()


#%%
#%%
# Plot training & validation loss values_
plt.plot(norm_histories_model_5.history['accuracy'])
plt.plot(norm_histories_model_5.history['val_accuracy'])

plt.title('Model_5_norm_BatchNormalization_Regul_3 accuracy')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.savefig("plots_PyCharm/accuracy_model_5_3_BatchNormalization_filtered_norm.png")
plt.show()
#%%
#############
#############


#%%
# MODEL 6
#%%
# Chenge kernal_weight_decay = 0.001
# activity_weight_decay = 0.001

#%% Define parameters
kernel_size = (row_n, 1)
n_filters = 8
x_train_shape = x_train.shape[1:4]
# Conv2D_dim_1 = (1, 3)
# Conv2D_n_1 = 64
pool_size_1 = (1, 3)
Dense_n_1 = 32
# my_optimizer = 'adam'
my_optimizer = keras.optimizers.Adam(learning_rate=0.0001)
my_loss = 'binary_crossentropy'
kernal_weight_decay = 0.01
activity_weight_decay = 0.001

my_epochs = 100

# In[68]:

from keras.models import Sequential
from keras.layers import *
from tensorflow.keras import regularizers

model_6 = Sequential()
model_6.add(
    Conv2D(filters=n_filters, kernel_size=kernel_size, strides=1, activation='relu', input_shape=x_train_shape, use_bias=True))
model_6.add(MaxPooling2D(pool_size=pool_size_1))

# second block
model_6.add(Conv2D(16, (1, 3), activation='relu', padding='same'))
model_6.add(MaxPooling2D((1, 2), padding='same'))
# third block
model_6.add(Conv2D(32, (1, 3), activation='relu', padding='same'))
model_6.add(MaxPooling2D((1, 2), padding='same'))

model_6.add(Flatten())

model_6.add(Dense(Dense_n_1, activation='relu',
             kernel_regularizer= regularizers.l1(kernal_weight_decay),
             activity_regularizer= regularizers.l1(activity_weight_decay)))
# add a batch normalization layer
model_6.add(BatchNormalization())
model_6.add(Dropout(0.25))

model_6.add(Dense(1, activation='sigmoid'))  # changed

#%%
model_6.compile(optimizer=my_optimizer, loss=my_loss, metrics=["accuracy"])

#%%
###############
###############
#%%

from keras.callbacks import EarlyStopping

early_stopping = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=10)

norm_histories_model_6 = model_6.fit(x_train_norm, y_train, callbacks=[early_stopping],
                                               epochs=my_epochs, verbose=1, shuffle=True, validation_split = 0.2)
# kernal_weight_decay = 0.001
# activity_weight_decay = 0.001
# 12791/12791 [==============================] - 4s 276us/step - loss: 0.5463 - accuracy: 0.7343 - val_loss: 0.8726 - val_accuracy: 0.5557
# Epoch 00016: early stopping
#%%
#%%
# Evaluate the model on the test data using `evaluate`
print("Evaluate on test data")
results_model_6 = model_6.evaluate(x_test_norm, y_test)
print("test loss, test acc:", results_model_6)
# # kernal_weight_decay = 0.001
# test loss, test acc: [0.8278528018377017, 0.5560280084609985]
# test loss, test acc: [0.8276286254291716, 0.5712856650352478]

# %%

#%%
# Plot training & validation loss values_
plt.plot(norm_histories_model_6.history['loss'])
plt.plot(norm_histories_model_6.history['val_loss'])

plt.title('Model_6_norm_BatchNormalization_Regul_3 loss')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.savefig("plots_PyCharm/Loss_model_6_BatchNormalization_filtered_norm.png")
plt.show()


#%%
#%%
# Plot training & validation loss values_
plt.plot(norm_histories_model_6.history['accuracy'])
plt.plot(norm_histories_model_6.history['val_accuracy'])

plt.title('Model_6_norm_BatchNormalization_Regul_3 accuracy')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.savefig("plots_PyCharm/accuracy_model_6_BatchNormalization_filtered_norm.png")
plt.show()





























# In[69]:

# history = model.fit(x_train, y_train, epochs=my_epochs, verbose=1, shuffle=True, validation_split=0.2)
# 12791/12791 [==============================] - 2s 185us/step - loss: 0.0731 - accuracy: 0.9737 - val_loss: 2.6192 - val_accuracy: 0.5491
#
from keras.callbacks import EarlyStopping

early_stopping = EarlyStopping(monitor='val_loss', patience=2)
model.fit(x, y, validation_split=0.2, callbacks=[early_stopping])
history_early_stoping = model.fit(x_train, y_train, callbacks=[early_stopping], epochs=my_epochs, verbose=1, shuffle=True,
                                  validation_split=0.2)
# %%




import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from keras.callbacks import EarlyStopping
from keras.callbacks import ModelCheckpoint
from keras.callbacks import TensorBoard


my_callbacks = [EarlyStopping(patience=2), ModelCheckpoint(filepath='model.{epoch:02d}-{val_loss:.2f}.h5'),TensorBoard(log_dir='./logs')]

# my_callbacks = [EarlyStopping(patience=2),
#     ModelCheckpoint(filepath='model.{epoch:02d}-{val_loss:.2f}.h5'),
#     TensorBoard(log_dir='./logs')]

#%%
history_early_stoping = model.fit(x_train, y_train, callbacks=my_callbacks, epochs=my_epochs, verbose=1, shuffle=True, validation_split=0.2)
#%%
_, acc = model.evaluate(x_test, y_test, verbose=0)
print('Test Accuracy: %.3f' % (acc * 100))

history_early_stoping.history
# {'val_loss': [1.731210672758459, 1.8694222366310345, 1.9775195871016769],
# 'val_accuracy': [0.6347717046737671, 0.6350844502449036, 0.6310194134712219],
# 'loss': [0.006007132676013059, 0.003269654892346562, 0.0021888746748899074],
# 'accuracy': [1.0, 1.0, 1.0]}

#%%
# Evaluate the model on the test data using `evaluate`
print("Evaluate on test data")
results = model.evaluate(x_test, y_test, batch_size=128)
print("test loss, test acc:", results)
#%%

# In[80]:


# history.history.keys()
# # Out[88]: dict_keys(['val_loss', 'val_accuracy', 'loss', 'accuracy'])
# history.history['accuracy']


# In[77]:


import matplotlib.pyplot as plt
#get_ipython().run_line_magic('matplotlib', 'inline')
#%%

#%%


#%%






#%%
# MODEL 4
#%%
# Chenge kernal_weight_decay = 0.001
# activity_weight_decay = 0.001

#%% Define parameters
kernel_size = (row_n, 1)
n_filters = 32
x_train_shape = x_train.shape[1:4]
Conv2D_dim_1 = (1, 3)
Conv2D_n_1 = 64
pool_size_1 = (1,3)
Dense_n_1 = 64
my_optimizer = 'adam'
my_loss = 'binary_crossentropy'
kernal_weight_decay = 0.001
activity_weight_decay = 0.001

my_epochs = 16

# In[68]:


from keras.models import Sequential
from keras.layers import *
from tensorflow.keras import regularizers

model = Sequential()
model.add(
    Conv2D(filters=n_filters, kernel_size=kernel_size, strides=1, activation='relu', input_shape=x_train_shape, use_bias=True))
model.add(MaxPooling2D(pool_size=Conv2D_dim_1))
model.add(Flatten())

model.add(Dense(Dense_n_1, activation='relu',
             kernel_regularizer= regularizers.l2(kernal_weight_decay),
             activity_regularizer= regularizers.l1(activity_weight_decay)))
model.add(Dropout(0.25))

model.add(Dense(1, activation='sigmoid'))  # changed


# model.compile(optimizer='adam', loss='mse', metrics = ["accuracy"])
# model.compile(optimizer='sgd', loss=tf.keras.losses.BinaryCrossentropy(), metrics = ["accuracy"])
model.compile(optimizer=my_optimizer, loss=my_loss, metrics=["accuracy"])

# In[69]:

history = model.fit(x_train, y_train, epochs=my_epochs, verbose=1, shuffle=True, validation_split=0.2)

# In[80]:


history.history.keys()
# Out[88]: dict_keys(['val_loss', 'val_accuracy', 'loss', 'accuracy'])
history.history['accuracy']


# In[77]:


import matplotlib.pyplot as plt
#get_ipython().run_line_magic('matplotlib', 'inline')
#%%
# Plot training & validation accuracy values
plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
plt.title('Model_4 accuracy')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.savefig("Accuracy_model_4.png")
plt.show()
#%%
# Plot training & validation loss values
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('Model_4 loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.savefig("Loss_model_4.png")
plt.show()

#%%
from keras.callbacks import EarlyStopping
early_stopping = EarlyStopping(monitor='val_loss', patience=2)
#model.fit(x, y, validation_split=0.2, callbacks=[early_stopping])
history_norm = model.fit(x_train, y_train, callbacks=[early_stopping], epochs=15, verbose=1, shuffle = True, validation_split=0.2)



#%%
# MODEL 5
#%%
# Chenge kernal_weight_decay = 0.001
# activity_weight_decay = 0.001

#%% Define parameters
kernel_size = (row_n, 1)
n_filters = 8
x_train_shape = x_train.shape[1:4]
Conv2D_dim_1 = (1, 3)
Conv2D_n_1 = 16
pool_size_1 = (1,3)
Dense_n_1 = 64
my_optimizer = 'adam'
my_loss = 'binary_crossentropy'
kernal_weight_decay = 0.001
activity_weight_decay = 0.0001

my_epochs = 100

# In[68]:


from keras.models import Sequential
from keras.layers import *
from tensorflow.keras import regularizers

model = Sequential()
model.add(
    Conv2D(filters=n_filters, kernel_size=kernel_size, strides=1, activation='relu', input_shape=x_train_shape, use_bias=True))
model.add(MaxPooling2D(pool_size=Conv2D_dim_1))
model.add(Flatten())

model.add(Dense(Dense_n_1, activation='relu',
             kernel_regularizer= regularizers.l1(kernal_weight_decay),
             ))
model.add(Dropout(0.25))

model.add(Dense(1, activation='sigmoid'))  # changed


# model.compile(optimizer='adam', loss='mse', metrics = ["accuracy"])
# model.compile(optimizer='sgd', loss=tf.keras.losses.BinaryCrossentropy(), metrics = ["accuracy"])
model.compile(optimizer=my_optimizer, loss=my_loss, metrics=["accuracy"])

# In[69]:
#%%
from keras.callbacks import EarlyStopping
early_stopping = EarlyStopping(monitor='val_loss', patience=2)
#model.fit(x, y, validation_split=0.2, callbacks=[early_stopping])
history_norm = model.fit(x_train, y_train, callbacks=[early_stopping], epochs=15, verbose=1, shuffle = True, validation_split=0.2)
#%%

_, acc = model.evaluate(x_test, y_test, verbose=0)
print('Test Accuracy: %.3f' % (acc * 100))
#%%

history = model.fit(x_train, y_train, epochs=my_epochs, verbose=1, shuffle=True, validation_split=0.2)

# In[80]:


history.history.keys()
# Out[88]: dict_keys(['val_loss', 'val_accuracy', 'loss', 'accuracy'])
history.history['accuracy']


# In[77]:


import matplotlib.pyplot as plt
#get_ipython().run_line_magic('matplotlib', 'inline')
#%%
# Plot training & validation accuracy values
plt.plot(history_norm.history['accuracy'])
plt.plot(history_norm.history['val_accuracy'])
plt.title('Model_x accuracy')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
#plt.savefig("Accuracy_model_4.png")
plt.show()
#%%
# Plot training & validation loss values
plt.plot(history_norm.history['loss'])
plt.plot(history_norm.history['val_loss'])
plt.title('Model_X loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
#plt.savefig("Loss_model_4.png")
plt.show()

