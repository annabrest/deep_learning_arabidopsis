#!/usr/bin/env python
# coding: utf-8

# In[]:
# Load packages:

from __future__ import absolute_import, division, print_function, unicode_literals
import functools
import tensorflow as tf
import numpy as np
import pandas as pd
import glob #if you want to read files from global invironment
from sklearn.model_selection import train_test_split

# In[]:
# Read file with labels
path_to_output_data = "/Users/abrestovitsky/Work/2019/Analysis/Deep_learning_arabidopsis/output_data/"
# name's of the file with labels
file_name_labels = "CNN_expression_output_mean_apex_inflorescence_21d_AGI_filtered_8to12TPM.csv"
# name's of the file with labels for positive control
file_name_labels_post_cntr = "CNN_expression_output_positive_cntr.csv"
#

# In[]:
train_set_y_orig = pd.read_csv(path_to_output_data + file_name_labels)
train_set_y_post_cntr = pd.read_csv(path_to_output_data + file_name_labels_post_cntr)

# In[]:
type(train_set_y_orig)
# Out[21]: pandas.core.frame.DataFrame
type(train_set_y_post_cntr)
# <class 'pandas.core.frame.DataFrame'>
# In[]:

print(train_set_y_post_cntr.shape)
print(train_set_y_post_cntr.head(3))
# (26450, 2)
#      sequence  label
# 0  AT1G01010      1
# 1  AT1G01040      0
# 2  AT1G01110      0

train_set_y_post_cntr.sequence
train_set_y_post_cntr.iloc[:,[0]]
train_set_y_post_cntr.iloc[:,0]

# In[]:
# Read Input files

file_names_post_cntr = [s + ".csv" for s in np.array(train_set_y_post_cntr.sequence)]

# In[]:

folder_name_input = "CNN_input_matrix_df_bin_100bp/"

file_list_post_cntr = [path_to_output_data + folder_name_input + s for s in file_names_post_cntr]


#file_list = (path + file_names)
print(file_list_post_cntr[0:5])
# ['/Users/abrestovitsky/Work/2019/Analysis/Deep_learning_arabidopsis/output_data/CNN_input_matrix_df_bin_100bp/AT1G01010.csv',
# '/Users/abrestovitsky/Work/2019/Analysis/Deep_learning_arabidopsis/output_data/CNN_input_matrix_df_bin_100bp/AT1G01040.csv',
# '/Users/abrestovitsky/Work/2019/Analysis/Deep_learning_arabidopsis/output_data/CNN_input_matrix_df_bin_100bp/AT1G01110.csv',
# '/Users/abrestovitsky/Work/2019/Analysis/Deep_learning_arabidopsis/output_data/CNN_input_matrix_df_bin_100bp/AT1G01160.csv',
# '/Users/abrestovitsky/Work/2019/Analysis/Deep_learning_arabidopsis/output_data/CNN_input_matrix_df_bin_100bp/AT1G01160.csv']

# In[]:

dfs_post_cntr = np.array([pd.read_csv(fp).values for fp in file_list_post_cntr])
print(dfs_post_cntr.shape)
# (26450, 872, 15)

# In[]:
# Filter out TFs bound to more than 15000 genes
# Based on the calculation that have been done using R
Index_file = "CNN_input_matrix_df/TF_filtered_200to10000_index.csv"
TF_index = pd.read_csv(path_to_output_data + Index_file)
TF_index.columns = ["index"]

# In[]:
print(type(TF_index))
print(TF_index.head(3))
print(dfs_post_cntr[:,TF_index.index,:].shape)
# <class 'pandas.core.frame.DataFrame'>
#    index
# 0    248
# 1     53
# (26450, 755, 15)

# In[]: Update dfs_post_cntr to include info only for filtered TFs, based on index
dfs_post_cntr_filtered = dfs_post_cntr[:,TF_index.index,:]

# In[]: Normalise by dividing by 100

dfs_post_cntr_filtered_norm = dfs_post_cntr_filtered/100

print('not normalised min=%.3f, max=%.3f' % (dfs_post_cntr_filtered.min(), dfs_post_cntr_filtered.max()))
print('normalised min=%.3f, max=%.3f' % (dfs_post_cntr_filtered_norm.min(),dfs_post_cntr_filtered_norm.max()))
# not normalised min=0.000, max=99.996
# normalised min=0.000, max=1.000

# In[]:

row_n = dfs_post_cntr_filtered_norm.shape[1]
col_n = dfs_post_cntr_filtered_norm.shape[2]

print("n of rows", row_n, "n of columns", col_n)
# n of rows 755 n of columns 15
# In[]:
train_set_y_post_cntr.head(3)

# In[]:
# Define training set
Y_train_post_cntr = (np.array(train_set_y_post_cntr['label'])).reshape(train_set_y_post_cntr['label'].shape[0], 1)

# X_train = dfs_filtered.reshape((train_set_y_orig.shape[0]), row_n, col_n, 1)
# X_train_norm = dfs_filtered_norm.reshape((train_set_y_orig.shape[0]), row_n, col_n, 1)

X_train_post_cntr = dfs_post_cntr_filtered_norm.reshape((Y_train_post_cntr.shape[0]), row_n, col_n, 1)

# In[]:

print ("number of training examples = " + str(X_train_post_cntr.shape[0]))
print ("X_train shape: " + str(X_train_post_cntr.shape))
print ("Y_train shape: " + str(Y_train_post_cntr.shape))
# number of training examples = 26450
# X_train shape: (26450, 755, 15, 1)
# Y_train shape: (26450, 1)

# In[]:
# Split data set into "train" and "test":
# set the seed by "random_state=11"

x_train, x_test, y_train, y_test = train_test_split(X_train_post_cntr, Y_train_post_cntr, test_size = 0.2, random_state=11) #set the seed by "random_state=11"

# acheck new dim of the data set after split:
print ("number of training examples = " + str(x_train.shape[0]))
print ("x_train shape: " + str(x_train.shape))
print ("y_train shape: " + str(y_train.shape))

# number of training examples = 21160
# x_train shape: (21160, 755, 15, 1)
# y_train shape: (21160, 1)
####

# In[]:

from keras.utils import to_categorical
from keras.models import Sequential
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Dense
from keras.layers import Flatten
from keras.preprocessing.image import ImageDataGenerator

from tensorflow import keras
#from tensorflow.keras import layers

# In[]:
#
# *MODEL 2*
# In[]:
# Define parameters
kernel_size = (row_n, 1)
n_filters = 32
x_train_shape = x_train.shape[1:4]
Conv2D_dim_1 = (1, 3)
Conv2D_n_1 = 64
pool_size_1 = (1,3)
Dense_n_1 = 64
# my_optimizer = keras.optimizers.Adam(learning_rate=0.0001)

lr_schedule = keras.optimizers.schedules.ExponentialDecay(
    initial_learning_rate=1e-2,
    decay_steps=10000,
    decay_rate=0.9)
my_optimizer = keras.optimizers.SGD(learning_rate=lr_schedule)


my_loss = 'binary_crossentropy'
# tf.keras.losses.BinaryCrossentropy()
my_epochs = 16

# In[]:

from keras.models import Sequential
from keras.layers import *

model_2 = Sequential()
model_2.add(
    Conv2D(filters=n_filters, kernel_size=kernel_size, strides=1, activation='relu', input_shape=x_train_shape, use_bias=True))
model_2.add(MaxPooling2D(pool_size=Conv2D_dim_1))
model_2.add(Flatten())
model_2.add(Dense(Dense_n_1, activation='relu'))

model_2.add(Dense(1, activation='sigmoid'))  # changed
# model.compile(optimizer='adam', loss='mse', metrics = ["accuracy"])
# model.compile(optimizer='sgd', loss=tf.keras.losses.BinaryCrossentropy(), metrics = ["accuracy"])
model_2.compile(optimizer=my_optimizer, loss=my_loss, metrics=["accuracy"])

# In[]:

histories_post_cntr = {}
histories_post_cntr['model_2'] = model_2.fit(x_train, y_train, epochs=my_epochs, verbose=1, shuffle=True, validation_split=0.2)
#16928/16928 [==============================] - 3s 167us/step - loss: 0.5195 - accuracy: 0.7410 - val_loss: 0.5528 - val_accuracy: 0.7150

# In[]:
import matplotlib.pyplot as plt
# In[]:
data_to_plot = histories_post_cntr['model_2']
plot_name = 'Model_2_positive_control'
file_name_accuracy = "plots_PyCharm/Accuracy_model_2_post_cntr.png"
file_name_loss = "plots_PyCharm/loss_model_2_post_cntr.png"

# In[]:
# Plot training & validation accuracy values for normalised and non-normalised data
plt.plot(data_to_plot.history['accuracy'])
plt.plot(data_to_plot.history['val_accuracy'])
plt.plot(data_to_plot.history['accuracy'])
plt.plot(data_to_plot.history['val_accuracy'])

plt.title(plot_name)
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.savefig(file_name_accuracy)
plt.show()

# In[]:

plt.plot(data_to_plot.history['loss'])
plt.plot(data_to_plot.history['val_loss'])
plt.plot(data_to_plot.history['loss'])
plt.plot(data_to_plot.history['val_loss'])

plt.title(plot_name)
plt.ylabel('loss')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.savefig(file_name_loss)
plt.show()

############

# In[]:
my_epochs = 100
# history = model.fit(x_train_norm, y_train, epochs=my_epochs, verbose=1, shuffle=True, validation_split=0.2)
#
from keras.callbacks import EarlyStopping

# early_stopping = EarlyStopping(monitor='val_loss', patience=2)
early_stopping = EarlyStopping(monitor='val_binary_crossentropy', patience=200)
# model.fit(x, y, validation_split=0.2, callbacks=[early_stopping])
histories_post_cntr['model_2_100_epo'] = model_2.fit(x_train, y_train, callbacks=[early_stopping],
                                               epochs=my_epochs, verbose=1, shuffle=True, validation_split = 0.2)


#%%
# In[]:
data_to_plot = histories_post_cntr['model_2_100_epo']
plot_name = 'Model_2_positive_control'
file_name_accuracy = "plots_PyCharm/Accuracy_model_2_post_cntr_100.png"
file_name_loss = "plots_PyCharm/loss_model_2_post_cntr_100.png"

# In[]:
# Plot training & validation accuracy values for normalised and non-normalised data
plt.plot(data_to_plot.history['accuracy'])
plt.plot(data_to_plot.history['val_accuracy'])
plt.plot(data_to_plot.history['accuracy'])
plt.plot(data_to_plot.history['val_accuracy'])

plt.title(plot_name)
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.savefig(file_name_accuracy)
plt.show()

# In[]:

plt.plot(data_to_plot.history['loss'])
plt.plot(data_to_plot.history['val_loss'])
plt.plot(data_to_plot.history['loss'])
plt.plot(data_to_plot.history['val_loss'])

plt.title(plot_name)
plt.ylabel('loss')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.savefig(file_name_loss)
plt.show()



#%%
# Evaluate the model on the test data using `evaluate`
print("Evaluate on test data")

results_model_2 = model_2.evaluate(x_test, y_test, batch_size=128)

print("test loss, test acc:", results_model_2 )
# test loss, test acc: [0.6306696977191701, 0.6306816935539246, 0.7226843237876892]
#%%

model_2.summary()

#%%
# MODEL 6
#%%
# Chenge kernal_weight_decay = 0.001
# activity_weight_decay = 0.001

#%% Define parameters
kernel_size = (row_n, 1)
n_filters = 8
x_train_shape = x_train.shape[1:4]
# Conv2D_dim_1 = (1, 3)
# Conv2D_n_1 = 64
pool_size_1 = (1, 3)
Dense_n_1 = 32
# my_optimizer = 'adam'
my_optimizer = keras.optimizers.Adam(learning_rate=0.0001)
my_loss = 'binary_crossentropy'
kernal_weight_decay = 0.001
activity_weight_decay = 0.001

my_epochs = 100

# In[68]:

from keras.models import Sequential
from keras.layers import *
from tensorflow.keras import regularizers

model_6 = Sequential()
model_6.add(
    Conv2D(filters=n_filters, kernel_size=kernel_size, strides=1, activation='relu', input_shape=x_train_shape, use_bias=True))
model_6.add(MaxPooling2D(pool_size=pool_size_1))

# second block
model_6.add(Conv2D(16, (1, 3), activation='relu', padding='same'))
model_6.add(MaxPooling2D((1, 2), padding='same'))
# third block
model_6.add(Conv2D(32, (1, 3), activation='relu', padding='same'))
model_6.add(MaxPooling2D((1, 2), padding='same'))

model_6.add(Flatten())

model_6.add(Dense(Dense_n_1, activation='relu',
             kernel_regularizer= regularizers.l1(kernal_weight_decay),
             activity_regularizer= regularizers.l1(activity_weight_decay)))
# add a batch normalization layer
model_6.add(BatchNormalization())
model_6.add(Dropout(0.25))

model_6.add(Dense(1, activation='sigmoid'))  # changed

#%%
model_6.compile(optimizer=my_optimizer, loss=my_loss, metrics=["accuracy"])

#%%
###############
###############
#%%

from keras.callbacks import EarlyStopping

early_stopping = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=10)

histories_post_cntr['model_6_100_epo']  = model_6.fit(x_train, y_train, callbacks=[early_stopping],
                                               epochs=my_epochs, verbose=1, shuffle=True, validation_split = 0.2)
# kernal_weight_decay = 0.001
# activity_weight_decay = 0.001
# 16928/16928 [==============================] - 4s 218us/step - loss: 0.4690 - accuracy: 0.8045 - val_loss: 0.5949 - val_accuracy: 0.7209
# Epoch 00021: early stopping

#%%
# Evaluate the model on the test data using `evaluate`
print("Evaluate on test data")
results_model_6 = model_6.evaluate(x_test, y_test)
print("test loss, test acc:", results_model_6)
# # kernal_weight_decay = 0.001
# test loss, test acc: [0.5966491220804153, 0.7147448062896729]

# In[]:
data_to_plot = histories_post_cntr['model_6_100_epo']
plot_name = 'Model_6_positive_control'
file_name_accuracy = "plots_PyCharm/Accuracy_model_6_post_cntr_100.png"
file_name_loss = "plots_PyCharm/loss_model_6_post_cntr_100.png"

# In[]:
# Plot training & validation accuracy values for normalised and non-normalised data
plt.plot(data_to_plot.history['accuracy'])
plt.plot(data_to_plot.history['val_accuracy'])
plt.plot(data_to_plot.history['accuracy'])
plt.plot(data_to_plot.history['val_accuracy'])

plt.title(plot_name)
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.savefig(file_name_accuracy)
plt.show()

# In[]:

plt.plot(data_to_plot.history['loss'])
plt.plot(data_to_plot.history['val_loss'])
plt.plot(data_to_plot.history['loss'])
plt.plot(data_to_plot.history['val_loss'])

plt.title(plot_name)
plt.ylabel('loss')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.savefig(file_name_loss)
plt.show()


